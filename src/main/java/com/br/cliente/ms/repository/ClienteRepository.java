package com.br.cliente.ms.repository;

import com.br.cliente.ms.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}
