package com.br.cliente.ms.service;

import com.br.cliente.ms.exceptions.ClienteNaoEncontradoException;
import com.br.cliente.ms.models.Cliente;
import com.br.cliente.ms.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente create(Cliente cliente) {

        return clienteRepository.save(cliente);
    }

    public Cliente getById(Long id) {
        Optional<Cliente> byId = clienteRepository.findById(id);

        if(!byId.isPresent()) {
            throw new ClienteNaoEncontradoException();
        }

        return byId.get();
    }
}
